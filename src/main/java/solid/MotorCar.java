package solid;

public class MotorCar implements Car {
    private Engine engine;
    public MotorCar(){
        this.engine = new Engine();
    }
    public void start() {
        System.out.println("I am a Gas car");
        engine.run();
    }

    public void accelerate(int kmh) {
        System.out.println("I am a Diesel car");
        engine.speed(kmh);
    }

    public void stop() {
        System.out.println("I am a classic car");
        engine.stop();
    }
}

package solid;

public interface Car {
    void start();
    void accelerate(int kmh);
    void stop();

}

package solid;

public class ElectricCar implements Car {

    private Engine engine;

    public ElectricCar() {
        this.engine = new Engine();
    }



    public void start() {
        System.out.println("no noise");
        engine.run();
    }

    public void accelerate(int kmh) {
        System.out.println("Voltage");
        engine.speed(kmh);
    }

    public void stop() {
        System.out.println("Electric Brakes");
    engine.stop();
    }

    public void regenerativeBraking(){
        engine.stop();
        System.out.println("Regenerating braking");
    }


}

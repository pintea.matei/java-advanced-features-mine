import solid.Car;
import solid.ElectricCar;
import solid.MotorCar;

public class Main {

    public static  Car car;

    public static void drive(Car car){
        car.start();
        car.accelerate(100);
        car.stop();
    }
    public static void main(String[] args) {
        System.out.println("Functioneaza");

        car = new MotorCar();
        Car electricCar = new ElectricCar();

        drive(car);
        drive(electricCar);

        ElectricCar carSpecific = new ElectricCar();
        specificDrive(carSpecific);
    }

    public static void specificDrive(ElectricCar electricCar){
        electricCar.start();
        electricCar.accelerate(23);
        electricCar.stop();
        electricCar.regenerativeBraking();
    }

}

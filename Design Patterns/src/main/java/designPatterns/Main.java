package designPatterns;

import designPatterns.factories.AbstractFactory;
import designPatterns.factories.PizzaFactory;
import designPatterns.pizza.Pizza;
import designPatterns.pizza.PizzaType;

public class Main {
    public static void main(String[] args) {
        AbstractFactory pizzaFactory = new PizzaFactory();
        Pizza comanda1 = pizzaFactory.create(PizzaType.QUATROFORMAGI,30);
        Pizza comanda2 = pizzaFactory.create(PizzaType.MARGHERITA, 25);
        Pizza comanda3 = pizzaFactory.create(PizzaType.QUARTROSTAGIONI, 50);

        System.out.println(comanda1);
        System.out.println(comanda2);
        System.out.println(comanda3);
    }

}

package designPatterns.factories;

import designPatterns.pizza.*;

public class PizzaFactory implements AbstractFactory {
    @Override
    public Pizza create(PizzaType type, int size) {
        switch (type) {
                case MARGHERITA:
                    return new PizzaMargerita(size);

                 case QUATROFORMAGI:
                     return new PizzaQuatroFromagi(size);

                case QUARTROSTAGIONI:
                    return new PizzaQuatroStagioni(size);

            default: return null;
        }

    }
}

package designPatterns.pizza;

public class PizzaQuatroStagioni extends Pizza {
    private int size;

    public PizzaQuatroStagioni(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.QUARTROSTAGIONI.toString();
    }

    @Override
    public String getIngredients() {
        return "blat, mozzarela, sos de rosii, salam, ciuperci";
    }

    @Override
    public int getSize() {
        return size;
    }
}


package designPatterns.pizza;

public class PizzaMargerita extends Pizza {
    private int size;

    public PizzaMargerita(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.MARGHERITA.toString();
    }

    @Override
    public String getIngredients() {
        return "blat, mozzarela, sos de rosii, busuioc";
    }

    @Override
    public int getSize() {
        return size;
    }
}

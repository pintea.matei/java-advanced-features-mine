package designPatterns.pizza;

public class PizzaQuatroFromagi extends Pizza {
    private int size;

    public PizzaQuatroFromagi(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.QUATROFORMAGI.toString();
    }

    @Override
    public String getIngredients() {
        return "blat, mozzarela, sos de rosii, gorgonzola, parmzan, brie";
    }

    @Override
    public int getSize() {
        return size;
    }
}


package fluentInterface;


public class Restaurant {
    private String name;
    private Menu menu;

    public Restaurant() {
        this.menu = new FoodMenu();
    }

    public Restaurant name(String name){
        this.name = name;
        System.out.println("Enter to restaurant " + name);
        return this;
    }
    public Menu getMenu(){
        return menu;
    }
}



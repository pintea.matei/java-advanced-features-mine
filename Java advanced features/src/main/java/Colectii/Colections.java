package Colectii;

import java.util.*;

public class Colections {
    public class Main {
    }
    public static void main(String[] args) {
        ArrayList<String> visitedCountries = new ArrayList<>();
        visitedCountries.add("Germany");
        visitedCountries.add("France");
        visitedCountries.add("Spain");
        for (String country : visitedCountries) {
            System.out.println(country + " ");
        }
        System.out.println("Number of contries: " + visitedCountries.size());
        System.out.println();
        visitedCountries.remove("France");

        for (String country : visitedCountries) {
            System.out.println(country + " ");
        }
        System.out.println("Number of contries: " + visitedCountries.size());
        System.out.println();

        List<String> contries = new ArrayList<>();
        contries.add("Italy");
        contries.add("Romania");
        contries.add("Portugalia");
        contries.add("Rusia");
        visitedCountries.addAll(contries);


        System.out.println("-------------------");

        Iterator<String> iterator2 = contries.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }

        System.out.println("-------------------");

        for (String country : visitedCountries) {
            if (country.startsWith("R")) {
                System.out.println(country);
            }
        }
        System.out.println("-------------------");
        for (String country : visitedCountries) {
            if (country.contains("r") || country.contains("R")) {
                System.out.println(country);
            }
        }
        System.out.println("****************");
        Iterator<String> iterator1 = visitedCountries.iterator();
        while (iterator1.hasNext()) {
            //    System.out.println(iterator1.next());
            String country = iterator1.next();
            if (country.contains("m")) {
                System.out.println(country);
            }
        }
        System.out.println("****************");
        visitedCountries.add(3, "Patagonia");
        for (String country : visitedCountries) {
            System.out.print(country + " ");
        }
        System.out.println(visitedCountries.size());
        for (int i = 0; i < visitedCountries.size() - 1; i++) {
            if (visitedCountries.get(i + 1).startsWith("R")) {
                System.out.println(visitedCountries.get(i));
            }
        }
        System.out.println();
        visitedCountries.remove(2);
        for (String country : visitedCountries) {
            System.out.print(country + " ");
        }

        System.out.println();
        visitedCountries.remove("Patagonia");
        for (String country : visitedCountries) {
            System.out.print(country + " ");
        }
        System.out.println();
        visitedCountries.add("Romania");
        for (String country : visitedCountries) {
            System.out.print(country + " ");
        }
        System.out.println("-----Lista 1 culori--------");
        final Set<String> color1 = new HashSet<>();
        color1.add( "red");
        color1.add( "blue");
        color1.add( "green");
        color1.add( "yellow");

        System.out.println("Lista nr 1 de culori este: ");
        for (String color : color1) {
            System.out.print(color + ", ");
        }
        System.out.println();

        System.out.println("-----Stergem culoare green--------'");
        color1.remove("green");
        for (String color : color1) {
            System.out.print(color + ", ");
        }
        System.out.println();

        System.out.println("---Agaugam o culoare deja existenta in lista--------");
        color1.add("blue");
        for (String color : color1) {
            System.out.print(color + ", ");
        }

        System.out.println();
        System.out.println("-----Lista 2 culori--------");
        final Set<String> color2 = new TreeSet<>();
        color2.add("purple");
        color2.add("pink");
        color2.add("grey");
        color2.add("orange");
        color2.add("pink");
        color2.add("white");
        for (String color : color2) {
            System.out.print(color + ", ");
        }
        System.out.println();
        System.out.println("-----Am adaugat o culoare--------");
        color2.add("black");
        for (String color : color2) {
            System.out.print(color + ", ");
        }
        System.out.println();
        System.out.println("-----Am adaugat o culoare deja existenta: orange--------");
        color2.add("orange");
        for (String color : color2) {
            System.out.print(color + ", ");
        }
        System.out.println();
        System.out.println("-----Am sters o culoare :grey--------");
        color2.remove("grey");
        for (String color : color2) {
            System.out.print(color + ", ");
        }
    }

}


